const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './tests/*_test.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'http://www.qa2.ru',
      show: false,
      browser: 'chromium',
    },
    ResembleHelper : {
      require: "codeceptjs-resemblehelper",
      baseFolder: "./visualization/base/",
      diffFolder: "./visualization/diff/"
    }
  },
  include: {
    I: './steps_file.js',
    mainPage: './pages/mainPage.js',
    registrationPage: './pages/registration.js',
    authorizationPage: './pages/authorization.js',
    searchPage: './pages/search.js',
    productCell: './fragments/product.js',
    alertFragment: './fragments/alert.js',
    personalPage: './pages/personalArea.js',
    cartPage: './pages/cart.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'qaBooksTesting',
  plugins: {
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
    testomat: {
      enabled: true,
      require: '@testomatio/reporter/lib/adapter/codecept',
      apiKey: process.env.TESTOMATIO,
    }
  }
}