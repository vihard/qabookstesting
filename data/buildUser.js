const faker = require('faker');

const BuildUser = function () {
    this.getUser = function () {
        const userdata = {
            name: faker.name.firstName(),
            surname: faker.name.lastName(),
            email: faker.internet.email(),
            tel: faker.phone.phoneNumber(),
            pass: faker.internet.password(),
            address: faker.address.streetAddress(),
            city: faker.address.city(),
        };
        return userdata;
    };
    this.updateUser = function () {
        const newuser = {
            name: faker.name.firstName(),
            tel: faker.phone.phoneNumber(),
        };
        return newuser;
    };
}

module.exports = new BuildUser();
