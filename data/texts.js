const SUCCSESS_REGISTER = 'Ваша учетная запись создана!';
const SUCCSESS_LOGIN = 'Моя учетная запись';
const SUCCESS_SEARCH = 'Поиск - ';
const UNSUCCESS_SEARCH = 'Нет товаров, которые соответствуют критериям поиска.';
const SUCCESS_INFO_CHANGE = 'Ваша учетная запись была успешно обновлена!';
const EMPTY_HISTORY = 'У Вас не было транзакций!';
const SUCCESS_SUBSCRIBE = 'Ваша подписка успешно обновлена!';
const SUCCSESS_ORDER = 'Ваш заказ принят!';

module.exports = { SUCCSESS_REGISTER, SUCCSESS_LOGIN, SUCCESS_SEARCH, UNSUCCESS_SEARCH, SUCCESS_INFO_CHANGE, EMPTY_HISTORY, SUCCESS_SUBSCRIBE, SUCCSESS_ORDER };
