'use strict';

let I;

module.exports = {

    _init() {
        I = actor();
    },

    root: '.product-thumb',
    productTitle: {
        xpath: '//div[@class="caption"]//a',
    },
    productPrice: {
        css: '.price',
    },
    buyBtn: {
        xpath: '//span[contains(text(),"Купить")]',
    },
    likeBtn: {
        css: '[data-original-title="В закладки"]',
    },
    compareBtn: {
        css: '[data-original-title="В сравнение"]',
    },

    async getProductTitle() {
        return within(this.root, async () => {
            I.waitForElement(this.productTitle);
            return await I.grabTextFrom(this.productTitle);
        });
    },

    async getProductPrice() {
        return within(this.root, async () => {
            I.waitForElement(this.productPrice);
            const price = await await I.grabTextFrom(this.productPrice);
            return price;
        });
    },

    async seeProduct() {
        return within(this.root, async () => {
            const result = await tryTo(() => I.waitForElement(this.root));
            return result;
        });
    },

    async addProductToCart() {
        return within(this.root, async () => {
            I.waitForElement(this.buyBtn);
            I.click(this.buyBtn);
            I.waitForText('добавлен в корзину покупок');
        });
    },
}
