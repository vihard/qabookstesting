const { I } = inject();

module.exports = {
    buttons: {
        login: 'input[type="submit"]',
    },
    fields: {
        email: '#input-email',
        password1: '#input-password',
    },

    enterCredentials(email, pass) {
        I.waitForText('Войти в Личный Кабинет');
        I.fillField({ css: this.fields.email }, email);
        I.fillField({ css: this.fields.password1 }, pass);
    },

    submitLogin() {
        I.click({ css: this.buttons.login });
    },

    async getHeadText() {
        I.waitForText('Личный Кабинет');
        const heading = await I.grabTextFrom({ xpath: '//h2[1]' });
        return heading;
    }

}


