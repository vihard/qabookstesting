const { I } = inject();
const { SEARCH_SMTH_URL } = require('../data/urls');

module.exports = {
    buttons: {
        searchBtn: '.input-group-btn',
        personalArea: '//a[@data-toggle="dropdown"]',
        goToRegister: '//a[contains(text(), "Регистрация")]',
        goToLogin: '//a[contains(text(), "Авторизация")]',
        cart: '#cart-total',
        info: '//h5[contains(text(),"Информация")]',
    },
    fields: {
        search: '[name="search"]',
    },

    smokeTest() {
        I.waitForText('Рекомендуемые');
        I.waitForElement({ xpath: this.buttons.personalArea });
        I.click({ css: this.buttons.cart });
        I.waitForText ('Ваша корзина пуста!');
        I.click({ css: this.buttons.cart });
        I.scrollTo({ xpath: this.buttons.info });
        I.waitForText('О нас');
    },

    goToRegister() {
        I.click({ xpath: this.buttons.personalArea });
        I.click({ xpath: this.buttons.goToRegister });
        I.waitInUrl(REGISTER_URL);
        I.waitForText('Регистрация');
    },

    goToLogin() {
        I.click({ xpath: this.buttons.personalArea });
        I.click({ xpath: this.buttons.goToLogin });
        I.waitInUrl(LOGIN_URL);
        I.waitForText('Зарегистрированный клиент');
    },

    goToSearch(keyword) {
        I.waitForElement({ css: this.fields.search }, 3);
        I.fillField({ css: this.fields.search }, keyword);
        I.click({ css: this.buttons.searchBtn });
        I.waitInUrl(`${SEARCH_SMTH_URL}=${keyword}`);
    },
}
