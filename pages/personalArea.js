const { I } = inject();

module.exports = {
    buttons: {
        continue: '.btn-primary',
        cart: '#cart-total',
        gotoCartBtn: '//strong/i[@class="fa fa-shopping-cart"]',
    },
    links: {
        changeInfo: '//li/a[contains(text(), "Изменить контактную информацию")]',
        historyView: '//li/a[contains(text(), "История транзакций")]',
        subscription: '//li/a[contains(text(), "Подписаться или отказаться")]',
    },
    fields: {
        name: '#input-firstname',
        phone: '#input-telephone',
    },
    radios: {
        checkSubscr: '//input[@value="1"][@checked="checked"]',
        noSubscr: '//input[@value="0"]',
        yesSubscr: '//input[@value="0"]',
    },

    changeContactInfo(newName, newTel) {
        I.waitInUrl('/my-account');
        I.waitForText('Моя учетная запись');
        I.click({ xpath: this.links.changeInfo});
        I.waitForText('Учетная запись');
        I.clearField({ css: this.fields.name});
        I.fillField({ css: this.fields.name}, newName);
        I.clearField({ css: this.fields.phone});
        I.fillField({ css: this.fields.phone}, newTel);
        I.click(this.buttons.continue);
        I.waitForText('Моя учетная запись');
    },

    viewTransactionsHistory() {
        I.waitInUrl('/my-account');
        I.waitForText('Мои заказы');
        I.click({ xpath: this.links.historyView});
        I.waitForText('История транзакций');
    },

    async changeNewsletter() {
        I.waitInUrl('/my-account');
        I.waitForText('Подписка');
        I.click({ xpath: this.links.subscription });
        const statusYes = await tryTo(() => I.waitForElement({ xpath: this.radios.checkSubscr }));
        if (statusYes) {
            I.click({ xpath: this.radios.noSubscr });
        } else {
            I.click({ xpath: this.radios.yesSubscr });
        }
        I.click({ css: this.buttons.continue });
        I.waitForText('Моя учетная запись');
    },

    gotoCart() {
        I.click({ css: this.buttons.cart });
        I.click({ xpath: this.buttons.gotoCartBtn });
        I.waitForText('Корзина покупок');
    },



}


