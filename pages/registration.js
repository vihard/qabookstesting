const { I } = inject();

module.exports = {
    buttons: {
        continue: 'input[type="submit"]',
    },
    fields: {
        firstName: '#input-firstname',
        lastName: '#input-lastname',
        email: '#input-email',
        tel: '#input-telephone',
        address: '#input-address-1',
        city: '#input-city',
        password1: '#input-password',
        password2: '#input-confirm',
    },
    selects: {
        country: '#input-country',
        zone: '#input-zone',
        moscow: 'option[value="50"]'
    },
    checkboxes: {
        agree: 'input[name="agree"]',
    },

    enterBasicData(name, secName, email, tel) {
        I.waitForText('Основные данные');
        I.fillField({ css: this.fields.firstName }, name);
        I.fillField({ css: this.fields.lastName }, secName);
        I.fillField({ css: this.fields.email }, email);
        I.fillField({ css: this.fields.tel }, tel);
    },

    enterAddress(address, city) {
        I.waitForText('Ваш адрес');
        I.fillField({ css: this.fields.address }, address);
        I.fillField( { css: this.fields.city }, city);
        I.selectOption({ css: this.selects.zone }, 'Москва');
    },

    enterPassword(password) {
        I.waitForText('Ваш пароль');
        I.fillField({ css: this.fields.password1 }, password);
        I.fillField( { css: this.fields.password2 }, password);
    },

    checkAgreeOption() {
        I.waitForText('согласен с условиями');
        I.checkOption({ css: this.checkboxes.agree });
    },

    submitRegistration() {
        I.click({ css: this.buttons.continue });
    },

    async getHeadText() {
        I.waitForText('Успешно');
        const heading = await I.grabTextFrom({ xpath: '//h1' });
        return heading;
    }

}


