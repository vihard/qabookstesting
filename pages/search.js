const { I } = inject();

module.exports = {
    buttons: {
        searchBtn: '#button-search',
    },
    fields: {
        search: '[placeholder="Ключевые слова"]',
    },
    selects: {},
    checkboxes: {
        inDescription: '#description',
    },

    enterKeyword(keyword) {
        I.fillField({ css: this.fields.search }, keyword);
        I.fillField( { css: this.fields.city }, city);
        I.selectOption({ css: this.selects.zone }, 'Москва');
    },

    submitSearch() {
        I.click({ css: this.buttons.searchBtn });
    },

    async getHeadText() {
        I.waitForText('Товары, соответствующие критериям поиска');
        const heading = await I.grabTextFrom({ xpath: '//h1' });
        return heading;
    },

    async getSadText() {
        I.waitForText('Товары, соответствующие критериям поиска');
        const heading = await I.grabTextFrom({ xpath: '//h1' });
        return heading;
    }





}


