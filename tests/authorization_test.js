const assert = require('assert').strict;
const user = require('../data/сredentials.json');
const { SUCCSESS_LOGIN } = require('../data/texts');
const { BASE_URL } = require('../data/urls');
const { mainPage, authorizationPage } = inject();

Feature('Authorization');

Before(({ I })=> {
    I.amOnPage(BASE_URL);
});

Scenario('User authorization @feature', async ({ I}) => {
    mainPage.goToLogin();
    authorizationPage.enterCredentials(user.email, user.password);
    authorizationPage.submitLogin();
    const headText = await authorizationPage.getHeadText();
    assert.equal(headText, SUCCSESS_LOGIN);
});
