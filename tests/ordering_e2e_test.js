const assert = require('assert').strict;
const user = require('../data/сredentials.json');
const { SUCCSESS_ORDER } = require('../data/texts');
const { BASE_URL } = require('../data/urls');
const { mainPage, authorizationPage, productCell, personalPage, cartPage } = inject();

Feature('Ordering');

Before(async ({ I })=> {
    I.amOnPage(BASE_URL);
    mainPage.goToLogin();
    authorizationPage.enterCredentials(user.email, user.password);
    authorizationPage.submitLogin();
    await cartPage.clearCart();
});

Scenario('User places an order @e2e', async ({ I}) => {
    const sought = 'путь';
    mainPage.goToSearch(sought);
    productCell.addProductToCart();
    personalPage.gotoCart();
    cartPage.createOrder();
    I.waitInUrl('/success');
    const headText = await cartPage.getHeadText();
    assert.equal(headText, SUCCSESS_ORDER);
});
