let BuildUser = require ('../data/buildUser');
const assert = require('assert').strict;
const { SUCCESS_INFO_CHANGE, EMPTY_HISTORY, SUCCESS_SUBSCRIBE } = require('../data/texts');
const { BASE_URL, } = require('../data/urls');
const { mainPage, authorizationPage, personalPage, productCell, cartPage } = inject();

const user = require('../data/сredentials.json');

Feature('Inside personal area');

Before(({ I })=> {
    I.amOnPage(BASE_URL);
    mainPage.goToLogin();
    authorizationPage.enterCredentials(user.email2, user.password2);
    authorizationPage.submitLogin();
});

Scenario('Change user info @feature', async ({ I}) => {
    I.amOnPage('/my-account');
    const newU = BuildUser.updateUser();
    personalPage.changeContactInfo(newU.name, newU.tel);
    const isOk = await tryTo(() => I.waitForText(SUCCESS_INFO_CHANGE));
    assert.ok(isOk, 'Произошла ошибка при смене контактной информации.')
});

Scenario('View transactions history @feature', async ({ I}) => {
    I.amOnPage('/my-account');
    personalPage.viewTransactionsHistory();
    const isOk = await tryTo(() => I.waitForText(EMPTY_HISTORY));
    assert.ok(isOk, 'Произошла ошибка при просмотре транзакций.')
});

Scenario('Change newsletter @feature', async ({ I}) => {
    I.amOnPage('/my-account');
    await personalPage.changeNewsletter();
    const isOk = await tryTo(() => I.waitForText(SUCCESS_SUBSCRIBE));
    assert.ok(isOk, 'Произошла ошибка при изменении статуса подписки.')

});

Scenario('Order amount check @feature', async ({ I}) => {
    await cartPage.clearCart();
    const sought = 'путь';
    mainPage.goToSearch(sought);
    productCell.addProductToCart();
    const realPrice = await productCell.getProductPrice();
    personalPage.gotoCart();
    I.waitInUrl('/cart');
    const cartAmount = await I.grabTextFrom(cartPage.elements.amount);
    assert.equal(realPrice, cartAmount);
});